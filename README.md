# Py Floppy Copy

Intended usecase is the sony mavica. It downloads all images from a floppy disk. Optional it can download all files.

- Linux <br>
![Linux](.media/demo_linux.gif)
- Windows <br>
![Windows](.media/demo_windows.gif)


## Getting started

```bash
sudo pip3.10 install -r requierements.txt
```
```bash
cd src
sudo python3.10 main.py
```

