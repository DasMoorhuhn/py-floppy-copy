import os
import shutil
import piexif
import subprocess

from datetime import datetime
from time import time
from progressbar.progressbar import ProgressBar
from platform import system

from settings import CAMERA_MODEL
from settings import CAMERA_MAKE
from settings import FLOPPY_DISK_PATH
from settings import FILE_FORMATS
from settings import USER
from settings import CHMOD
from settings import DST_FOLDER


def ls_floppy_disk():
  """List the files on the floppy disk"""
  try:return os.listdir(path=FLOPPY_DISK_PATH)
  except:return


def disk_info():
  """Prints information about the floppy disk"""
  disk_usage = shutil.disk_usage(FLOPPY_DISK_PATH)
  files = ls_floppy_disk()

  if files is None:
    print(f'No files found on {FLOPPY_DISK_PATH}')
    return

  print(f'Total: {round(disk_usage.total / 1024 / 1024, 2)}MB')
  print(f'Free: {round(disk_usage.free / 1024 / 1024, 2)}MB ({round(disk_usage.free * 100 / disk_usage.total, 2)}%)')
  print(f'Used: {round(disk_usage.used / 1024 / 1024, 2)}MB ({round(disk_usage.used * 100 / disk_usage.total, 2)}%)')
  print(f'Files Total: {len(files)}')
  print()


def chown_chmod(dst):
  """Assigns the folder/file to the user"""
  if system() not in ['Linux', 'Darwin']: return
  subprocess.call(['sudo', 'chown', USER, dst], stdout=subprocess.DEVNULL)
  subprocess.call(['sudo', 'chmod', CHMOD, dst], stdout=subprocess.DEVNULL)


def modify_exif(dst, creation_date):
  """Modifies the exif data of the image since the mavica doesn't save the creation date and the camera information in the image"""
  img = piexif.load(dst)

  img['0th'][piexif.ImageIFD.Make] = CAMERA_MAKE
  img['0th'][piexif.ImageIFD.Model] = CAMERA_MODEL
  img['0th'][piexif.ImageIFD.DateTime] = creation_date
  img['Exif'][piexif.ExifIFD.DateTimeOriginal] = creation_date
  img['Exif'][piexif.ExifIFD.DateTimeDigitized] = creation_date

  exif_bytes = piexif.dump(img)
  piexif.insert(exif_bytes, dst)


def copy_files_from_floppy_disk(files_list):
  """Starts the copy process of the files from src to dst"""
  counter = 0
  file_list = []
  for file in files_list:
    if FILE_FORMATS[0] == '*' or file.split('.')[-1].upper() in FILE_FORMATS: file_list.append(file)

  file_counter = len(file_list)
  print(f'Found {file_counter} files')

  date = datetime.fromtimestamp(time()).strftime('%Y_%m_%d_%H-%M-%S')
  dst_path = os.path.join(DST_FOLDER, f'{date}')
  if not os.path.exists(dst_path): os.makedirs(dst_path)
  chown_chmod(dst=dst_path)

  pg_bar = ProgressBar(maxval=file_counter, term_width=70).start()
  for file in file_list:
    src = os.path.join(FLOPPY_DISK_PATH, file)
    dst = os.path.join(dst_path, file)

    creation_date = datetime.fromtimestamp(os.stat(src).st_ctime).strftime("%Y:%m:%d %H:%M:%S")

    shutil.copyfile(src=src, dst=dst)
    chown_chmod(dst=dst)
    modify_exif(dst=dst, creation_date=creation_date)

    counter += 1
    pg_bar.update(counter)

  pg_bar.finish()
