import os
import subprocess

from platform import system

from settings import FLOPPY_DISK_PATH
from settings import FLOPPY_DEVICE


def create_mount_folder():
  """Create the mount folder where the floppy disk will be mounted it. Only needed for Linux/Darwin"""
  if not os.path.exists(FLOPPY_DISK_PATH):
    os.makedirs(FLOPPY_DISK_PATH)
    if not os.path.exists(FLOPPY_DISK_PATH): exit(1)


def __unix_mount():
  create_mount_folder()
  subprocess.call(['sudo', 'mount', FLOPPY_DEVICE, FLOPPY_DISK_PATH],
                  stdout=subprocess.DEVNULL,
                  stderr=subprocess.DEVNULL)
  return os.path.ismount(FLOPPY_DISK_PATH)


def __unix_umount():
  subprocess.call(['sudo', 'umount', FLOPPY_DISK_PATH],
                  stdout=subprocess.DEVNULL,
                  stderr=subprocess.DEVNULL)
  return os.path.ismount(FLOPPY_DISK_PATH)


def __windows_mount():
  return os.path.exists('A:')


def mount():
  """Mount the floppy disk to the mounting destination.

  :returns:
    True if the drive is mounted, else False
  """
  match system():
    case 'Linux' | 'Darwin': return __unix_mount()
    case 'Windows': return __windows_mount()


def umount():
  """Unmount the floppy disk to the mounting destination.

    :returns:
      True if the drive is mounted, else False
    """
  match system():
    case 'Linux' | 'Darwin': return __unix_umount()
    case 'Windows': return __windows_mount()
