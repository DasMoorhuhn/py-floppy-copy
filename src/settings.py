# General
FILE_FORMATS = ['JPG', 'MPG']                        # Only download these filetypes. If it has * on first place, it will download all files.
USER_NAME = 'user'                                   # Your users name.
CAMERA_MAKE = 'Sony_Mavica'                          #
CAMERA_MODEL = 'FD95'                                #

# Linux/Darwin
FLOPPY_DISK_PATH = f'/media/{USER_NAME}/disk'       # The path, where the floppy disk will be mounted on.
FLOPPY_DEVICE = '/dev/sde'                          # The path of the device itself.
DST_FOLDER = f'/home/{USER_NAME}/Bilder/FloppyCopy' # The destination folder, where the files will be coppied in.
UID = 1000                                          # Your users user ID.
GID = 1000                                          # Your users group ID.
USER = f'{UID}:{GID}'                               #
CHMOD = '755'  # https://chmod-calculator.com/      # The rights which will be given a downloaded file.

# Windows
FLOPPY_DEVICE_WINDOWS = 'A:'                        # The drive letter of the floppy disk drive. Windows' default is A:
DST_FOLDER_WINDOWS = f'C:\\users\\{USER_NAME}\\pictures\\FloppyCopy'

# Don't touch this
from platform import system
VERSION = '2024.09.18'
match system():
  case 'Linux' | 'Darwin': pass
  case 'Windows':
    FLOPPY_DISK_PATH = FLOPPY_DEVICE_WINDOWS
    DST_FOLDER = DST_FOLDER_WINDOWS
  case _:
    print(f"You OS ({system()}) is not valid")
    exit(1)
