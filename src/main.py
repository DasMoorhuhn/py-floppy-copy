# https://gitlab.com/sony-mavica-tools


from time import sleep

from settings import VERSION
from settings import FLOPPY_DISK_PATH
from file_helper import ls_floppy_disk
from file_helper import copy_files_from_floppy_disk
from file_helper import disk_info
from mount_helper import mount
from mount_helper import umount


print(f'v{VERSION}')
WAITING = False  # Indicates, if the program is waiting for a floppy disk
MOUNTED = False  # Indicates, if the floppy disk is mounted

while True:
  if WAITING and not MOUNTED:
    MOUNTED = mount()

  if not WAITING and not MOUNTED:
    print('Waiting for floppy disk...')
    WAITING = True

  if WAITING and MOUNTED:
    WAITING = False
    print('Floppy disk detected\n')
    ls = ls_floppy_disk()

    if ls is None or len(ls) == 0:
      print(f'No files found on {FLOPPY_DISK_PATH}')
      umount()
      MOUNTED = False
    else:
      disk_info()
      copy_files_from_floppy_disk(ls)
      umount()
      MOUNTED = False
    input('Eject floppy disk and swap new in. Press Enter to continue...\n')

  # Your floppy disk drive will thank me for not asking a few thousand times per second, if it's mounted or not
  sleep(1)
